package br.com.desafio.inter;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.desafio.inter.controller.UserRequest;
import br.com.desafio.inter.model.User;
import br.com.desafio.inter.service.UserService;

@RunWith(SpringRunner.class)
public class UserTest {
	
//	@TestConfiguration
//	static class testeConfiguration {
//		
//		@Bean
//		public UserService userService() {
//			return new UserService();
//		}
//	}
	
	@Autowired(required = false)
	private UserService service;
	
	@Test
	public void createUser() {
		UserRequest userRequest = getUser();
		try {
			this.service.create(userRequest);
			
			User user = service.findByMail(userRequest.getMail());
			
			Assertions.assertNotNull(user);
			Assertions.assertEquals(user.getMail(), userRequest.getMail());
			Assertions.assertEquals(user.getName(), userRequest.getName());
			
			
		} catch (Exception e) {
			Assert.fail();
		}
		
	}
	
	@Test
	public void getFirstUser() {
		UserRequest userRequest = getUser();
		User user = service.getUserById(1L);
		
		Assertions.assertNotNull(user);
		Assertions.assertEquals(user.getMail(), userRequest.getMail());
		Assertions.assertEquals(user.getName(), userRequest.getName());
		
	}
	
	@Test
	public void simpleDigitWithTwoParameters() {
	Integer result = service.singleDigit("123", 3);
	
	Assertions.assertNotNull(result);
	Assertions.assertTrue(result != 0);
	Assertions.assertTrue(result.equals(123123123));
	
	}
	
	@Test
	public void simpleDigitWithTwoDigit() {
		Integer result = service.singleDigit(12);
		
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.equals(3));
	}
	
	@Test
	public void simpleDigit() {
		Integer result = service.singleDigit(1);
		
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.equals(1));
	}
	
	@Test public void updateUser() {
		UserRequest userRequest = getUser();
		
		try {
			 User user =this.service.create(userRequest);
			 userRequest.setMail("emailupdate@test.com");
			 userRequest.setName("Atualizado");
			 
			User userUpdate = this.service.update(user.getId(), userRequest);
			
			Assertions.assertNotNull(userUpdate);
			Assertions.assertNotNull(userRequest);
			Assertions.assertTrue(userUpdate.getName().equals(userRequest.getName()));
			Assertions.assertTrue(userUpdate.getMail().equals(userRequest.getMail()));
			
		} catch (Exception e) {
			Assertions.fail();
		}
		
		
	}

	private UserRequest getUser() {
		UserRequest user = new UserRequest();
		
		user.setName("Amigo Tester");
		user.setMail("Test@teste.com.br");
		return user;
	}

}
