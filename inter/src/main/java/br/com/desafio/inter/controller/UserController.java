package br.com.desafio.inter.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.inter.model.User;
import br.com.desafio.inter.service.UserService;
import br.com.desafio.inter.util.SingleDigit;
import io.swagger.annotations.Api;

@RestController
@RequestMapping(value = "/api")
@Api("API Crud de Usuários")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private UserService service;

	@GetMapping("users")
	public ResponseEntity<List<User>> getAll() {
		return new ResponseEntity<>(service.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping("user/{id}")
	public ResponseEntity<User> findUserById(@PathVariable("id") Long id) {
		try {
			User user = service.getUserById(id);
			return new ResponseEntity<>(user, HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("single-digit/{n}/{k}")
	public Integer singleDigit(@PathVariable("n") String n, @PathVariable("k") Integer k) {
		return service.singleDigit(n, k);
	}

	@GetMapping("single-digit/{x}")
	public Integer singleDigit(@PathVariable("x") Integer x) {
		return service.singleDigit(x);
	}
	
	@GetMapping("get-single-digits")
	public ResponseEntity<Map<SingleDigit, Integer>> getSingleDigits() {
		return new ResponseEntity<>(service.getSingleDigits(), HttpStatus.OK);
	}

	@PostMapping("user/create")
	public ResponseEntity<User> create(@RequestBody UserRequest request) {
		try {
			User user = service.create(request);
			return new ResponseEntity<>(user, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping("/user/update/{id}")
	public ResponseEntity<User> update(@PathVariable("id") Long id, @RequestBody UserRequest request) {
		try {
			User user = service.update(id, request);
			return new ResponseEntity<>(user, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@DeleteMapping("/user/delete/{id}")
	public void delete(@PathVariable("id") Long id) {
		service.delete(id);
	}

}
