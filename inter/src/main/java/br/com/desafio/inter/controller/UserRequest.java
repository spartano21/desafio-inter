package br.com.desafio.inter.controller;

import br.com.desafio.inter.util.SingleDigit;

public class UserRequest {
	private String name;
	private String mail;
	private SingleDigit simpleDigit;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}

	public SingleDigit getSimpleDigit() {
		return simpleDigit;
	}

	public void setSimpleDigit(SingleDigit simpleDigit) {
		this.simpleDigit = simpleDigit;
	}

}
