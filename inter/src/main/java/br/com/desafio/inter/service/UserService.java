package br.com.desafio.inter.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.inter.controller.UserRequest;
import br.com.desafio.inter.model.User;
import br.com.desafio.inter.repository.UserRepository;
import br.com.desafio.inter.util.SingleDigit;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private SingleDigit singleDigit;
	
//	@Autowired
//	private Cryptography cryptography;
	
	 Map<SingleDigit, Integer> digits;

	public List<User> getAllUsers() {
		return repository.findAll();
	}

	public User getUserById(Long id) {
		Optional<User> user = repository.findById(id);

		if (user != null && !user.isEmpty())
			return user.get();

		  throw new RuntimeException("Usuário inexistente");
	}
	
	public Integer singleDigit(Integer x) {
		return singleDigit.singleDigit(x);
	}

	public Integer singleDigit(String n, Integer k) {
		this.digits = new HashMap<>();
		Integer result = singleDigit.singleDigit(n, k);
		
		this.digits.put(new SingleDigit(n, k), result);
		return result ;
	}

	public void delete(Long id) {
		Optional<User> user = repository.findById(id);
		if (user != null && user.isEmpty()) {
			repository.delete(user.get());
		}
		
	}

	public User create(UserRequest request) {
		 User user = new User();
		 user.setName(request.getName());
		 user.setMail(request.getMail());
		 user.setDigits(Arrays.asList(new SingleDigit(request.getSimpleDigit().getN(), request.getSimpleDigit().getK())));
		 repository.save(user);
		 return user;
	}

	public User update(Long id, UserRequest request) {
		Optional<User> userOptional = repository.findById(id);
		if (userOptional != null) {
			User user = userOptional.get();
			user.setName(request.getName());
			user.setMail(request.getMail());
			user.setDigits( Arrays.asList(new SingleDigit(request.getSimpleDigit().getN(), request.getSimpleDigit().getK())));

			repository.save(user);
			return user;
		}
		throw new RuntimeException("Usuário inexistente!");
	}

	public User findByMail(String mail) {
		Optional<User> user = repository.findByMail(mail);
		if (user != null && !user.isEmpty()) {
			return user.get();
		}
		throw new RuntimeException("Usuário inexistente!");
	}

	public Map<SingleDigit, Integer> getSingleDigits() {
		return this.digits;
	}

}
