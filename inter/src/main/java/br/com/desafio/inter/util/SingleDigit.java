package br.com.desafio.inter.util;

import org.springframework.stereotype.Component;

@Component
public class SingleDigit {
	private String n;
	private Integer k;
	
	public SingleDigit() {
		
	}

	public SingleDigit(String n, Integer k) {
		this.n = n;
		this.k = k;
	}

	public Integer singleDigit(String n, Integer k) {
		String auxiliar = n;
		for(int i = 0; i< k-1; i++) {
			n+= auxiliar;
		}
		Integer p = (Integer.parseInt(n)) * k;
		return singleDigit(p);
	}

	public Integer singleDigit(Integer x) {
		return calculateSingleDigit(x);
	}

	private Integer calculateSingleDigit(Integer x) {
		String sigleDigit = Integer.toString(x);

		if (sigleDigit != null && sigleDigit.trim().length() > 1) {
			x = 0;
			for (Character c : sigleDigit.toCharArray()) {
				x += Character.getNumericValue(c);
			}
		}
		return x;

	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

}
