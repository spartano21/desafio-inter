package br.com.desafio.inter.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public Docket userApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.desafio.inter"))
				.paths(PathSelectors.regex("/api.*"))
				.build().apiInfo(metaInfo());
	}

	@SuppressWarnings("rawtypes")
	public ApiInfo metaInfo() {
		ApiInfo apiInfo = new ApiInfo("API Desafio Inter", "Unico digito, Crud usuarios e  criptografica", "1.0",
				"Developer", new Contact("Rafael Souza", "https://gitlab.com/spartano21", "rafaelhop@hotmail.com"),
				"Apache", "https://www.apache.org/licensen.html", new ArrayList<VendorExtension>());
		return apiInfo;
	}

}
